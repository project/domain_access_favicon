<?php

namespace Drupal\domain_access_favicon\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;

/**
 * DomainAccess Favicon SettingsForm.
 */
class DomainAccessFaviconSettingsForm extends ConfigFormBase {

  /**
   * Domain loader definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $domainLoader;

  /**
   * The storage handler class for files.
   *
   * @var \Drupal\file\FileStorage
   */
  protected $fileStorage;

  /**
   * Construct function.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($config_factory);

    $this->domainLoader = $entity_type_manager->getStorage('domain');
    $this->fileStorage = $entity_type_manager->getStorage('file');
  }

  /**
   * Create function return static entity type manager.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Load the ContainerInterface.
   *
   * @return \static
   *   return entity type manager configuration.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'domain_favicon_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['domain_access_favicon.settings'];
  }

  /**
   * Function for config domain favicon form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $domains = $this->domainLoader->loadMultiple();

    if ($domains) {
      $form['general'] = [
        '#type' => 'details',
        '#title' => $this->t('Domain Favicon Settings'),
        '#open' => TRUE,
      ];

      $domain_access_favicon_config = $this->config('domain_access_favicon.settings');

      // List of Image Extensions.
      $extensions = 'png gif jpg jpeg svg';

      foreach ($domains as $domain) {
        if ($domain->id()) {
          $form['general'][$domain->id()] = [
            '#type' => 'managed_file',
            '#title' => $this->t('Upload favicon for Domain: @hostname', ['@hostname' => $domain->label()]),
            '#description' => $this->t('The favicon for this domain/subdomain.<br>Allowed types: @extensions.', [
              '@extensions' => $extensions,
            ]),
            '#size' => 64,
            '#default_value' => $domain_access_favicon_config->get('favicons.' . $domain->id()),
            '#upload_validators'  => [
              'file_validate_extensions' => [$extensions],
            ],
            '#upload_location' => 'public://files',
            '#weight' => '0',
          ];
        }
      }
      return parent::buildForm($form, $form_state);
    }
    else {
      $url = Url::fromRoute('domain.admin');
      $domain_link = Link::fromTextAndUrl($this->t('Domain records'), $url)->toString();
      $form['title']['#markup'] = $this->t('There is no Domain record yet. Please create a domain records. See link: @domain_list', ['@domain_list' => $domain_link]);
      return $form;
    }
  }

  /**
   * Domain favicon config form submit.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('domain_access_favicon.settings');
    $domains = $this->domainLoader->loadOptionsList();
    foreach ($domains as $key => $value) {
      $config->set('favicons.' . $key, $form_state->getValue($key))->save();
      $image = $form_state->getValue($key);
      if (!empty($image)) {
        $file = $this->fileStorage->load($image[0]);
        if (!empty($file)) {
          $file->setPermanent();
          $file->save();
        }
      }
    }
    parent::submitForm($form, $form_state);
  }

}
