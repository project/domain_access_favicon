<?php

namespace Drupal\domain_access_favicon;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\domain\DomainNegotiatorInterface;

/**
 * Handy methods for the Domain Access Favicon.
 */
class DomainAccessFavicon {

  /**
   * The module config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The domain negotiator.
   *
   * @var \Drupal\domain\DomainNegotiatorInterface
   */
  protected $domainNegotiator;

  /**
   * The file URL generator.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected $fileUrlGenerator;

  /**
   * The file storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $fileStorage;

  /**
   * Constructs a DomainAccessFavicon object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\domain\DomainNegotiatorInterface $domain_negotiator
   *   The domain negotiator.
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $file_url_generator
   *   The file URL generator.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, DomainNegotiatorInterface $domain_negotiator, FileUrlGeneratorInterface $file_url_generator, EntityTypeManagerInterface $entity_type_manager) {
    $this->config = $config_factory->get('domain_access_favicon.settings');
    $this->domainNegotiator = $domain_negotiator;
    $this->fileUrlGenerator = $file_url_generator;
    $this->fileStorage = $entity_type_manager->getStorage('file');
  }

  /**
   * Return the active domain favicon.
   *
   * @return string
   *   The patch to the favicon.
   */
  public function getActiveDomainFavicon(): string {

    // Check if we have an active domain.
    $current_domain = $this->domainNegotiator->getActiveDomain();
    if (empty($current_domain)) {
      return '';
    }

    // Check if there is a favicon configured for the active domain.
    $favicon = $this->config->get('favicons.' . $current_domain->id());
    if (empty($favicon)) {
      return '';
    }

    // Check if we have an actual file.
    $file = $this->fileStorage->load($favicon[0]);
    if (empty($file)) {
      return '';
    }

    return $this->fileUrlGenerator->generateAbsoluteString($file->getFileUri());
  }

}
