# Domain Access Favicon

## Description

This module provides uploading different favicons for (sub)domains created using
the Domain module.

## Requirements

- [Domain](https://www.drupal.org/project/domain)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

Configuration for uploading a favicon for each (sub)domain:
`/admin/config/domain/domain_access_favicon`

This will update the favicon for each (sub)domain created through the Domain
module.

## Maintainers

- Rohit Tiwari - [rohit-tiwari](https://www.drupal.org/u/rohit-tiwari)
- Matthew Burdick - [matthew_burdick](https://www.drupal.org/u/matthew_burdick)
